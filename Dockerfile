## BUILD STAGE ##
FROM adoptopenjdk/openjdk11:latest AS builder

RUN echo $JAVA_HOME

COPY mvnw /build/mvnw
COPY .mvn /build/.mvn 
COPY src /build/src
COPY pom.xml /build
 
WORKDIR /build
 
RUN ./mvnw -B package -Dmaven.test.skip=true -Djar.name=app
 
## FINAL IMAGE ##
FROM adoptopenjdk/openjdk11:latest
 
ARG name=desafio-wallmart-backend
ARG version=1.0.0
ARG port=80

ENV INFO_APP_NAME=${name} \
    INFO_APP_VERSION=${version} \
    SERVER_PORT=${port}

COPY --from=builder /build/target/*.jar /opt/${name}/${version}/app.jar

WORKDIR /opt/${name}/${version}

EXPOSE $SERVER_PORT

CMD ["java", "-jar", "app.jar"]