package cl.veas.cerda.marco.desafiowalmart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import cl.veas.cerda.marco.desafiowalmart.exception.DesafioException;
import cl.veas.cerda.marco.desafiowalmart.model.data.Product;
import cl.veas.cerda.marco.desafiowalmart.response.Response;
import cl.veas.cerda.marco.desafiowalmart.service.ProductService;
import cl.veas.cerda.marco.desafiowalmart.utils.ProductUtils;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class ProductController {
	
	private static final String MINIMUS_CHARACTER_ERROR_MSG = "Debe tener al menos #number# caracteres o un Id de producto para realizar la búsqueda";
	
	@Autowired
	private ProductService productService;
	
	/**
	 * Variable que se obtiene de los properties con un valor entero que determina
	 * el porcentaje de descuento
	 */
	@Value("${offert.palindrome.percentage}")
	private int percentagePalindrome;

	/**
	 * Variable que se obtiene de los properties con un valor entero que determina
	 * la cantidad mímina de caracteres para realizar una búsqueda
	 */
	@Value("${minimus.character.search}")
	private int minimusCharacter;
	
	/**
	 * Método encargado de realizar la búsqueda y que responde al llamado get del
	 * servicio /product
	 * 
	 * @param string conjunto de caracter a buscar
	 * @return Respuesta con los resultado obtenidos de la busqueda
	 * @throws DesafioException Cuando no se cumple con la regla para realizar una
	 *                          búsqueda
	 */
	@GetMapping(value = "/product/{string}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> findProduct(@PathVariable("string") String string) throws DesafioException{
		
		if (string.length() < minimusCharacter && !ProductUtils.isInteger(string)) {
			throw new DesafioException(
					MINIMUS_CHARACTER_ERROR_MSG.replace("#number#", String.valueOf(minimusCharacter)),
					HttpStatus.BAD_REQUEST);
		}
						
		boolean isPalindrome = ProductUtils.isPalindrome(string);		
		log.info("Buscando expresión: {}, palindromo: {}", string, isPalindrome);
		List<Product> productsList = productService.findProduct(string);
		if(isPalindrome) {
			productsList.forEach(
					p -> p.setPrice(ProductUtils.getDiscount(p.getPrice(), percentagePalindrome)));
		}
		
		
		return new ResponseEntity<>(Response.builder()
					.isPalindrome(isPalindrome)
					.data(productsList)
					.build(),
				HttpStatus.OK);
	}

}
