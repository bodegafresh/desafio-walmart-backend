package cl.veas.cerda.marco.desafiowalmart.model.repository;

import java.util.List;

import cl.veas.cerda.marco.desafiowalmart.model.data.Product;

public interface ProductRespository {

	public Product findById(Integer id);

	public List<Product> findInDescription(String string);

	public List<Product> findInBrand(String string);

}
