package cl.veas.cerda.marco.desafiowalmart.utils;

/**
 * 
 * @author <a href="marco.cerda.veas@gmail.com">Marco Cerda Veas</a>
 *
 */
public class ProductUtils {

	/**
	 * Método que valida si es palindromo
	 * 
	 * @param string
	 * @return
	 */
	public static boolean isPalindrome(String string) {
		int fin = string.length() - 1;
		int ini = 0;
		boolean espalin = true;
		while (ini < fin) {
			if (string.charAt(ini) != string.charAt(fin)) {
				espalin = false;
			}
			ini++;
			fin--;
		}
		return espalin;
	}

	/**
	 * Método que obtiene el valor según el porcentaje que se pida
	 * 
	 * @param totalPrice precio total
	 * @param percentage porcentaje a aplicar
	 * @return
	 */
	public static int getDiscount(int totalPrice, int percentage) {
		return totalPrice - (totalPrice * percentage / 100);
	}
	
	/**
	 * Método que valida si un string es un entero
	 * @param s string
	 * @return true si es entero false si no
	 */
	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException | NullPointerException e) {
			return false;
		}
		return true;
	}

	private ProductUtils() {

	}

}
