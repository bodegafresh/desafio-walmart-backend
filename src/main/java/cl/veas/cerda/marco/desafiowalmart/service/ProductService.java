package cl.veas.cerda.marco.desafiowalmart.service;

import java.util.List;

import cl.veas.cerda.marco.desafiowalmart.model.data.Product;

public interface ProductService {
	
	public List<Product> findProduct(String string);

}
