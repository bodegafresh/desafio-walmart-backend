package cl.veas.cerda.marco.desafiowalmart.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class DesafioException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	private final HttpStatus httpStatus;
	
	public DesafioException(String msg, HttpStatus httpStatus) {
		super(msg);
		this.httpStatus = httpStatus;
	}

}
