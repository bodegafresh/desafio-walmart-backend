package cl.veas.cerda.marco.desafiowalmart.response;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mensaje;
	
	private HttpStatus httpStatus;

}
