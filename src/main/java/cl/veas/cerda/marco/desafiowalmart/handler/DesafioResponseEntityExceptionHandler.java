package cl.veas.cerda.marco.desafiowalmart.handler;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import cl.veas.cerda.marco.desafiowalmart.exception.DesafioException;
import cl.veas.cerda.marco.desafiowalmart.response.ErrorResponse;
import lombok.extern.log4j.Log4j2;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Log4j2
public class DesafioResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	protected ResponseEntity<ErrorResponse> responseGenericException(Exception ex){
		log.error(ex.getMessage(), ex);
		
		return ResponseEntity
	              .status(HttpStatus.INTERNAL_SERVER_ERROR)
	              .body(ErrorResponse.builder()
	      				.mensaje(ex.getMessage())
	    				.httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)	    				
	    				.build());
	}
	
	@ExceptionHandler(DesafioException.class)
	public ResponseEntity<ErrorResponse> responseKnownErrorException(DesafioException ex) {
		log.error(ex.getMessage(), ex);
		
		return ResponseEntity
	              .status(ex.getHttpStatus())
	              .body(ErrorResponse.builder()
	      				.mensaje(ex.getMessage())
	    				.httpStatus(ex.getHttpStatus())	    				
	    				.build());
	}

}
