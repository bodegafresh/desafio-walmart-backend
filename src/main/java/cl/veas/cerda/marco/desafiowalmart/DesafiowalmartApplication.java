package cl.veas.cerda.marco.desafiowalmart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafiowalmartApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafiowalmartApplication.class, args);
	}

}
