package cl.veas.cerda.marco.desafiowalmart.model.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import cl.veas.cerda.marco.desafiowalmart.model.data.Product;

@Repository
public class ProductRepository implements ProductRespository {

	@Autowired

	private final MongoTemplate mongoTemplate;

	@Autowired
	public ProductRepository(MongoTemplate mongoTemplate) {

		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public List<Product> findInDescription(String string) {
		Query query = new Query();		
		query.addCriteria(Criteria.where("description").regex(string));

		return mongoTemplate.find(query, Product.class);
	}

	@Override
	public List<Product> findInBrand(String string) {
		Query query = new Query();		
		query.addCriteria(Criteria.where("brand").regex(string));

		return mongoTemplate.find(query, Product.class);
	}

	@Override
	public Product findById(Integer id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		List<Product> resultado = mongoTemplate.find(query, Product.class);
		if (resultado.isEmpty()) {
			return null;
		}
		return resultado.get(0);
	}

}
