package cl.veas.cerda.marco.desafiowalmart.response;

import java.io.Serializable;
import java.util.List;

import cl.veas.cerda.marco.desafiowalmart.model.data.Product;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Response implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean isPalindrome;
	
	private List<Product> data;

}
