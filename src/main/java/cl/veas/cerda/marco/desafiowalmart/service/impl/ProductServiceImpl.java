package cl.veas.cerda.marco.desafiowalmart.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.veas.cerda.marco.desafiowalmart.model.data.Product;
import cl.veas.cerda.marco.desafiowalmart.model.repository.ProductRespository;
import cl.veas.cerda.marco.desafiowalmart.service.ProductService;
import cl.veas.cerda.marco.desafiowalmart.utils.ProductUtils;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRespository mongoRespository;

	@Override
	public List<Product> findProduct(String string) {
		List<Product> result = new ArrayList<>();
		if (ProductUtils.isInteger(string)) {
			Product product = mongoRespository.findById(Integer.parseInt(string));
			if (product != null) {
				result.add(product);
			} else {
				result.addAll(mongoRespository.findInDescription(string));
				result.addAll(mongoRespository.findInBrand(string));
			}
		} else {
			result.addAll(mongoRespository.findInDescription(string));
			result.addAll(mongoRespository.findInBrand(string));
		}
		return result.stream().distinct().collect(Collectors.toList());
	}

	

}
