package cl.veas.cerda.marco.desafiowalmart.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import cl.veas.cerda.marco.desafiowalmart.service.ProductService;

@WebMvcTest(controllers = ProductController.class)
class ProductControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ProductService productService;
		
	@Test
	void isPalindrome_testFindProduct() throws Exception {
		String palindrome = "asdfdsa";
		when(productService.findProduct(palindrome)).thenReturn(new ArrayList<>());
				
		this.mockMvc.perform(get("/product/" + palindrome))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.palindrome", is(true)));
	}
	
	@Test
	void notIsPalindrome_testFindProduct() throws Exception {
		String notPalindrome = "asdf";
		when(productService.findProduct(notPalindrome)).thenReturn(new ArrayList<>());
				
		this.mockMvc.perform(get("/product/" + notPalindrome))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.palindrome", is(false)));
	}
	
	@Test
	void limitCharacter_testFindProduct() throws Exception {
		String minusChartAccept = "as";
		
		this.mockMvc.perform(get("/product/" + minusChartAccept))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	void validId_testFindProduct() throws Exception {
		String idProduct = "1";
		when(productService.findProduct(idProduct)).thenReturn(new ArrayList<>());
		
		this.mockMvc.perform(get("/product/" + idProduct))
		.andExpect(status().isOk());
	}

}
