package cl.veas.cerda.marco.desafiowalmart.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ProductUtilsTest {

	@Test
	void is_testIsPalindrome() {
		String string = "asdfdsa";
		assertTrue(ProductUtils.isPalindrome(string));
	}

	@Test
	void no_testIsPalindrome() {
		String string = "as";
		assertFalse(ProductUtils.isPalindrome(string));
	}

	@Test
	void testGetDiscount() {
		int total = 200000;
		int resultado = 100000;

		assertEquals(resultado, ProductUtils.getDiscount(total, 50));
	}

	@Test
	void is_testIsInteger() {
		String aInteger = "1234";
		assertTrue(ProductUtils.isInteger(aInteger));
	}

	@Test
	void no_testIsInteger() {
		String aString = "1a2";
		assertFalse(ProductUtils.isInteger(aString));
	}

}
