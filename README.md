# Desafio Walmart Backend


### Prueba Full Stack Developer

Esta el backend para la prueba Full Stacka Develop de Walmart.

La solución fue desarrollada usando Java 8 con Spring boot 2.3.3.

- Autor: Marco Cerda Veas
- Email: [maro.cerda.veas@gmail.com](mailto:marco.cerda.veas@gmail.com) 
- LinkedIn: [Marco Cerda Veas](https://www.linkedin.com/in/marco-cerda-veas/)

## Requisitos

- Base de datos desde el repositorio [https://github.com/walmartdigital/products-db](https://github.com/walmartdigital/products-db)
- maven 3.6.* 
- java 8
- git version 2.15.0

## Properties

```properties
# Configuración de la base de datos
spring.data.mongodb.database=promotions
spring.data.mongodb.uri=mongodb://productListUser:productListPassword@localhost:27017/admin

# Porcentaje de descuento para las búsqueda de palindromos
offert.palindrome.percentage=50

# Determina la cantidad mímina de caracteres para realizar una búsqueda
minimus.character.search=3
```

## Ejecutar en local

```bash
mvn clean install -Dmaven.test.skip=true  spring-boot:run  
```

## Ejecutar pruebas

```bash
mvn test
```

